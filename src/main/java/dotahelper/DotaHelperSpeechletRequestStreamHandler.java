package dotahelper;

import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by lucaspedroza on 6/18/16.
 */
public class DotaHelperSpeechletRequestStreamHandler extends SpeechletRequestStreamHandler {
    private static final Set<String> supportedApplicationIds;

    static {
        /*
         * This Id can be found on https://developer.amazon.com/edw/home.html#/ "Edit" the relevant
         * Alexa Skill and put the relevant Application Ids in this Set.
         */
        supportedApplicationIds = new HashSet<String>();
        supportedApplicationIds.add("amzn1.echo-sdk-ams.app.8576e135-7914-4c03-a9eb-4979cc7d5457");
    }

    public DotaHelperSpeechletRequestStreamHandler() {
        super(new DotaHelperSpeechlet(), supportedApplicationIds);
    }

    public DotaHelperSpeechletRequestStreamHandler(Speechlet speechlet,
                                                   Set<String> supportedApplicationIds) {
        super(speechlet, supportedApplicationIds);
    }
}
