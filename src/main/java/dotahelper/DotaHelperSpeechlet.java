package dotahelper;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

import com.amazon.speech.ui.SimpleCard;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.amazonaws.util.json.JSONTokener;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lucaspedroza on 6/18/16.
 */

public class DotaHelperSpeechlet implements Speechlet {
    private static final Logger log = LoggerFactory.getLogger(DotaHelperSpeechlet.class);
    private static final String SLOT_HERO = "Hero";
    private static final String ENDPOINT = "http://24.56.212.211:8080/myapp";
    @Override
    public void onSessionStarted(SessionStartedRequest sessionStartedRequest, Session session) throws SpeechletException {

    }

    @Override
    public SpeechletResponse onLaunch(LaunchRequest launchRequest, Session session) throws SpeechletException {
        String speechOutput =
                "Welcome to the Dota Helper. You can ask me to start a Roshan timer, or what your last build was on a hero.";
        // If the user either does not reply to the welcome message or says
        // something that is not understood, they will be prompted again with this text.
        String repromptText = "For instructions on what you can say, please say help me.";

        return newAskResponse(speechOutput, repromptText);
    }

    @Override
    public SpeechletResponse onIntent(IntentRequest request, Session session) throws SpeechletException {

        Intent intent = request.getIntent();
        String intentName = (intent != null) ? intent.getName() : null;

        if ("RoshanIntent".equals(intentName)) {
            return handleStartRoshanTimerRequest(intent, session);
//        } else if ("AMAZON.HelpIntent".equals(intentName)) {
//            return getHelp();
        } else if ("RoshanStatusIntent".equals(intentName)) {
            return handleStatusRoshanTimerRequest(intent, session);
        } else if ("LastItemBuildIntent".equals(intentName)) {
            return handleLastItemBuildRequest(intent, session);
        }else if ("AMAZON.StopIntent".equals(intentName)) {
            PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
            outputSpeech.setText("Goodbye");

            return SpeechletResponse.newTellResponse(outputSpeech);
        } else if ("AMAZON.CancelIntent".equals(intentName)) {
            PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
            outputSpeech.setText("Goodbye");

            return SpeechletResponse.newTellResponse(outputSpeech);
        } else {
            throw new SpeechletException("Invalid Intent");
        }
    }

    private SpeechletResponse handleLastItemBuildRequest(Intent intent, Session session) {
        Slot heroSlot = intent.getSlot(SLOT_HERO);
        PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();

        if (heroSlot != null && heroSlot.getValue() != null) {
            String speechString = "Your most recent build was: ";
            String urlFragment = String.format("/recommend/items?hero=%s",heroSlot.getValue());
            JSONObject itemResponseObject = makeDotaRequest(urlFragment);
            try {

                if (itemResponseObject != null ) {
                    JSONArray items = (JSONArray)itemResponseObject.get("items");
                    speechString += items.toString();
                    outputSpeech.setText(speechString);
                } else {
                    speechString = "I'm not sure what happened, but it was horrible. I don't know what items you should use.";
                    outputSpeech.setText(speechString);
                }
            } catch (JSONException e) {
                log.error("Exception occurred while parsing service response.", e);
            }
        } else {
            outputSpeech.setText(String.format("Maybe that hero doesn't exist"));
        }
        return SpeechletResponse.newTellResponse(outputSpeech);

    }

    private SpeechletResponse handleStartRoshanTimerRequest(Intent intent, Session session) {
        String speechString = "";
        String urlFragment = String.format("/roshan/start?user=%s",session.getUser().getUserId());
        JSONObject roshanResponseObject = makeDotaRequest(urlFragment);

        try {
            if (roshanResponseObject != null) {
                RoshanResponse roshanResponse = new RoshanResponse(
                        roshanResponseObject.get("success").toString(),
                        roshanResponseObject.get("message").toString(),
                        roshanResponseObject.get("timeRemaining").toString(),
                        roshanResponseObject.get("timerActive").toString()
                );
                if (roshanResponse.success == "true") {
                    speechString = String.format("New Roshan timer started! Roshan might respawn in %s seconds",roshanResponse.timeRemaining);
                } else  {
                    speechString = String.format("A new Roshan timer was not started. Sorry.");
                }
            } else {
                speechString =
                        "Sorry, the Dota Helper service is experiencing a problem. "
                                + "Please try again later.";
            }
        } catch (JSONException je) {
            log.error("Exception occurred while parsing service response.", je);
        }

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Roshan Timer");
        card.setContent(speechString);

        // Create the plain text output
        PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
        outputSpeech.setText(speechString);

        return SpeechletResponse.newTellResponse(outputSpeech);
    }

    private SpeechletResponse handleStatusRoshanTimerRequest(Intent intent, Session session) {
        String speechString = "";

        String urlFragment = String.format("/roshan/status?user=%s", session.getUser().getUserId());
        JSONObject roshanResponseObject = makeDotaRequest(urlFragment);
        try {
            if (roshanResponseObject != null) {
                RoshanResponse roshanResponse = new RoshanResponse(
                        roshanResponseObject.get("success").toString(),
                        roshanResponseObject.get("message").toString(),
                        roshanResponseObject.get("timeRemaining").toString(),
                        roshanResponseObject.get("timerActive").toString()
                );
                if (roshanResponse.success == "true" && roshanResponse.timerActive == "true") {
                    speechString = String.format("Roshan might respawn in %s seconds",roshanResponse.timeRemaining);
                } else if (roshanResponse.success == "true" && roshanResponse.timerActive == "false") {
                    speechString = String.format("Leena cannot find an active Roshan timer for you.");
                }
            } else {
                speechString =
                        "Sorry, the Dota Helper service is experiencing a problem. "
                                + "Please try again later.";
            }
        } catch (JSONException je) {
            log.error("Exception occurred while parsing service response.", je);
        }

        // Create the Simple card content.
        SimpleCard card = new SimpleCard();
        card.setTitle("Roshan Timer");
        card.setContent(speechString);

        // Create the plain text output
        PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
        outputSpeech.setText(speechString);

        return SpeechletResponse.newTellResponse(outputSpeech);
    }

    private JSONObject makeDotaRequest(String urlFragment) {

        InputStreamReader inputStream = null;
        BufferedReader bufferedReader = null;
        StringBuilder builder = new StringBuilder();
        try {
            String line;
            URL url = new URL(ENDPOINT + urlFragment);
            inputStream = new InputStreamReader(url.openStream(), Charset.forName("US-ASCII"));
            bufferedReader = new BufferedReader(inputStream);
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            // reset builder to a blank string
            builder.setLength(0);
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(bufferedReader);
        }

        if (builder.length() == 0) {
            return null;
        } else {
            try {
                JSONObject dotaResponseObject = new JSONObject(new JSONTokener(builder.toString()));
                if (dotaResponseObject != null) {
                    return dotaResponseObject;
                }
            } catch (JSONException e) {
                log.error("Exception occoured while parsing service response.", e);
            }
        }
        return null;
    }

    @Override
    public void onSessionEnded(SessionEndedRequest sessionEndedRequest, Session session) throws SpeechletException {

    }

    private static class ItemRecommendationResponse implements IDotaHelperResponse {
        private final String success;
        private final String message;
        private final String[] items;

        ItemRecommendationResponse(String success, String message, String[] items) {
            this.success = success;
            this.message = message;
            this.items = items;
        }
    }

    private static class RoshanResponse implements IDotaHelperResponse {
        private final String success;
        private final String message;
        private final String timeRemaining;
        private final String timerActive;

        RoshanResponse(String success, String message, String timeRemaining, String timerActive) {
            this.success = success;
            this.message = message;
            this.timeRemaining = timeRemaining;
            this.timerActive = timerActive;
        }
    }

    private static class DotaHelperResponse implements  IDotaHelperResponse {
        private final String success;
        private final String message;

        DotaHelperResponse(String success, String message) {
            this.success = success;
            this.message = message;
        }
    }

    private interface IDotaHelperResponse {
    }

    private SpeechletResponse newAskResponse(String stringOutput, String repromptText) {
        PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
        outputSpeech.setText(stringOutput);

        PlainTextOutputSpeech repromptOutputSpeech = new PlainTextOutputSpeech();
        repromptOutputSpeech.setText(repromptText);
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(repromptOutputSpeech);

        return SpeechletResponse.newAskResponse(outputSpeech, reprompt);
    }
}
